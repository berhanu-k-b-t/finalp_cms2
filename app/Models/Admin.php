<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use HasFactory;
    
    /**
     * Get all of the comments for the Admin
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function musuem_records()
    {
        return $this->hasMany(MuseumRecord::class);
    }

     public function user()
    {
        return $this->hasOne(UserAccount::class);
    }

}